package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

@SpringBootApplication
@RestController
// It will tell springboot that this application will function as an endpoint handling web requests
public class S1Application {

	//Annotations in Java Springboot marks classes for their intended functionalities.
	//Springboot upon startup scans for classes and assign behaviors based on their annotation
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}
	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot App";
	}
	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John")String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080?hi?name=Joe
		//We retrieve the value of the filed "name" from our URL
		//defaultValue is the fallback value when the query string or request param is empty
		//System.out.println(nameParameter);
		return "Hi! My name is " + nameParameter;
	}

	@GetMapping("/favoriteFood")
	public String myFavoriteFood(@RequestParam(value="food",defaultValue = "Sinigang")String foodName){
		return "Hi! my favorite food is " + foodName;
	}
	//2 ways of passing data through the URL using Java Springboot
	//Query String using Request Params - directly passing data into the URL
	//Path Variable us much smiliar to ExpressJS's req.params

	@GetMapping("/greeting/{name}")
	public String greeting(@PathVariable("name") String nameParams){
		return "hello" + nameParams;
	}

	ArrayList<String> enrollees = new ArrayList<>();
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="user",defaultValue="Jane Doe")String user){
		enrollees.add(user);
		System.out.println(enrollees);
		return "Welcome " + user;
	}

	@GetMapping("/getEnrollees")
	public Object getEnrollees(){
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String course(@PathVariable("id") String courseId){

		if(courseId.equals("java101")){
			return "Name: Java101, Schedule: MWF 8:00AM - 11:00AM, Price: PHP3000";
		} else if (courseId.equals("jsoop101")){
			return "Name: JS OOP101 Schedule: MWF 8:00AM - 11:00AM, Price: PHP3000";
		} else if (courseId.equals("sql101")){
			return "Name: SQL101, Schedule: MWF 8:00AM - 11:00AM, Price: PHP3000";
		} else {
			return "course cannot be found";
		}
	}
}
